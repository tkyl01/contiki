#include "exit.h"

u8 touchkey = 0; //按键值，按位表示

void EXITInit(void)
{
    GPIO_InitTypeDef	GPIO_InitStructure;
    EXTI_InitTypeDef    EXTI_InitStructure;
    NVIC_InitTypeDef    NVIC_InitStructure;
    
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO,ENABLE);
    
    //LSCL
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
    //RSCL
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB,&GPIO_InitStructure);
    
    //LSDO RSDO
         
    GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_3 | GPIO_Pin_11;//PB3\4\11
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; //设置成上拉输入
 	GPIO_Init(GPIOB, &GPIO_InitStructure);//初始化GPIO
    
    //GPIOB.3  左触摸 中断线以及中断初始化配置   下降沿触发
  	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource3);
  	EXTI_InitStructure.EXTI_Line = EXTI_Line3;	
  	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
  	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  	EXTI_Init(&EXTI_InitStructure);//根据EXTI_InitStruct中指定的参数初始化外设EXTI寄存器
    
    //GPIOB.11  右触摸 中断线以及中断初始化配置   下降沿触发
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource11);
    EXTI_InitStructure.EXTI_Line = EXTI_Line11;	
  	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
  	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  	EXTI_Init(&EXTI_InitStructure);//根据EXTI_InitStruct中指定的参数初始化外设EXTI寄存器
    NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;			    //使能外部中断通道3
  	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;	//抢占优先级2， 
  	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;			//子优先级1
  	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;					//使能外部中断通道
  	NVIC_Init(&NVIC_InitStructure);
    
    NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;			//使能外部中断通道11
  	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;	//抢占优先级2， 
  	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;			//子优先级2
  	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;					//使能外部中断通道
  	NVIC_Init(&NVIC_InitStructure);
}

//外部中断3服务程序  左触摸 
void EXTI3_IRQHandler(void)
{
    u8 i = 0;
    u8 keyval = 0;//按键值，按位表示
    if(EXTI_GetITStatus(EXTI_Line3) != RESET)
    {
        Delay1us(50);
        for(i = 0;i < 8;i ++)
        {
            LSCL = 0;
            Delay1us(10);
            LSCL = 1;
            Delay1us(10);
            keyval >>= 1;
            LSDO ? (keyval |= 0x00):(keyval |= 0x80);
        }
        if(keyval == 0x01)        touchkey = FILTER2KEY;
        else if(keyval == 0x02)   touchkey = FILTER1KEY;
        else if(keyval == 0x04)   touchkey = CHILDLOCKKEY;
        else if(keyval == 0x08)   touchkey = HUMIDIFICATIONKEY;
        else                      touchkey = NOEFFECTKEY;
        EXTI_ClearITPendingBit(EXTI_Line3); //清除LINE3上的中断标志位 
    }        
}

//外部中断11服务程序  右触摸
void EXTI15_10_IRQHandler(void)
{
    u8 i = 0;    u8 keyval = 0;//按键值，按位表示
    if(EXTI_GetITStatus(EXTI_Line11) != RESET)
    {
        Delay1us(50);
        for(i = 0;i < 8;i ++)
        {
            RSCL = 0;
            Delay1us(10);
            RSCL = 1;
            Delay1us(10);
            keyval >>= 1;
            RSDO ? (keyval |= 0x00):(keyval |= 0x80);
        }
        if(keyval == 0x01)        touchkey = WINDVELOCITYPLUSKEY;
        else if(keyval == 0x02)   touchkey = FOGAMOUNTKEY;
        else if(keyval == 0x04)   touchkey = TIMERPLUSKEY;
        else if(keyval == 0x08)   touchkey = SWITCHKEY;
        else                      touchkey = NOEFFECTKEY;
        EXTI_ClearITPendingBit(EXTI_Line11); //清除LINE11上的中断标志位
    }      
}




