#include "adc.h"



void ADCInit(void)
{
    ADCGPIOInit();
    Adc_Configuration();
}

void ADCGPIOInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
    
    //GPIOB.1模拟输入
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	
}

//ADC 通道配置
void  Adc_Configuration(void)
{ 	
	ADC_InitTypeDef ADC_InitStructure;
    
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE);//使能ADC1
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);//时钟6分频	
	ADC_DeInit(ADC1);//初始化默认ADC1
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;//ADC1 ADC2 工作在独立模式
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;    //单通道
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; //单次转换
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;//软件触发
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;//右对齐
	ADC_InitStructure.ADC_NbrOfChannel = 1;//顺序转换1个通道	
	ADC_Init(ADC1,&ADC_InitStructure);//初始化ADC1	 
	ADC_Cmd(ADC1,ENABLE);//使能ADC1		
	ADC_ResetCalibration(ADC1);//重置ADC1校准寄存器		 
	while(ADC_GetResetCalibrationStatus(ADC1))//获取重置ADC1校准寄存器状态			
	{
        printf("Resetting ADC!\n");
    }
    printf("ADC Reset OK!\n");
    ADC_StartCalibration(ADC1);//开始ADC1的校准状态	 
	while(ADC_GetCalibrationStatus(ADC1))//获取指定ADC的校准程序 返回校准新状态
    {
        printf("Fetching ADC new Status!\n");
    }
    printf("ADC init OK!\n");
}	

//读取 ADC通道值
u16 Get_Adc(u8 ch)   
{
	ADC_RegularChannelConfig(ADC1,ch,1,ADC_SampleTime_239Cycles5);	 
	ADC_SoftwareStartConvCmd(ADC1,ENABLE);
	while(!ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC));
	return ADC_GetConversionValue(ADC1);	
}

//读 times 次 返回平均值
u16 Get_Adc_Average(u8 ch,u8 times)
{
	u32 temp_val = 0;
	u8 t;
	for(t = 0;t < times;t ++)
	{
		temp_val += Get_Adc(ch);
		Delay1us(5);
	}
	return temp_val/times;
}
