#ifndef __MCP41010_H__
#define __MCP41010_H__

#include "stm32f10x.h"
#include "delay.h"
#include "system.h"

#define MCP41010_CS     PBout(9)    //Ƭѡ
#define MCP41010_CLK    PAout(1)    //ʱ��
#define MCP41010_SI     PAout(0)    //����

void Mcp41010Init(void);
void Write2Mcp41010Data(unsigned char cmd,unsigned char dat);

#endif
