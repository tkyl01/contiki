 /**
  ******************************************************************************
  * @file    TFTLCD/User/My/delay.h
  * @author  N-K-J
  * @version V3.5.0
  * @date    2012/12/11
  * @brief   delay.h
  ******************************************************************************
 **/
#ifndef __DELAY_H
#define __DELAY_H 			   
#include "stm32f10x.h"
/*------------------------------------------------------	 
使用SysTick的普通计数模式对延迟进行管理
包括delay_us,delay_ms
------------------------------------------------------*/

void DelayInit(u8 SYSCLK);
void Delay1ms(u16 nms);
void Delay1us(u32 nus);

#endif
