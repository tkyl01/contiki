#include "tim.h"

u8 TIM1ITCount1 = 0;//定时器10ms中断计数1 侧电机转速使用u8 TIM1ITCount2 = 0;//定时器10ms中断计数2 按键扫描使用
u8 TIM1ITCount3 = 0;//定时器10ms中断计数3 屏幕刷新使用
u8 TIM1ITCount4 = 0;//定时器10ms中断计数4 净化闪烁使用
u16 TIM1ITCount5 = 0;//定时器10ms中断计数5 倒计时使用
u8 TIM1ITCount6 = 0;//定时器10ms中断计数6 滤网使用
u8 TIM1ITCount7 = 0;//定时器10ms中断计数7 加湿使用

//
void TIM1Init(uint16_t TIM1Period,uint16_t TIM1Prescaler)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
#if TIM1_ITEN
	NVIC_InitTypeDef NVIC_InitStructure;
#endif

    TIM_DeInit(TIM1);//复位 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1,ENABLE); //时钟使能
                                              
	TIM_TimeBaseStructure.TIM_Period = TIM1Period;//自动重载计数值
	TIM_TimeBaseStructure.TIM_Prescaler = TIM1Prescaler;//分频系数 
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//时钟分频因子  不分频 系统时钟 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//计数方式 向上
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;////注意此处  高级定时器专用
    TIM_TimeBaseInit(TIM1,&TIM_TimeBaseStructure);

#if TIM1_ITEN
	TIM_ClearFlag(TIM1,TIM_FLAG_Update);      	//清除中断标志
	TIM_ITConfig(TIM1,TIM_IT_Update,ENABLE); 	//允许更新中断

	NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn; //TIM1 溢出中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01; //先占优先级 1 级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03; //从优先级 3 级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ 通道被使能
	NVIC_Init(&NVIC_InitStructure); //初始化 NVIC 寄存器
#endif
	TIM_Cmd(TIM1, ENABLE); //使能 TIMx 外设
}

//
void TIM2Init(uint16_t TIM2Period,uint16_t TIM2Prescaler)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
#if TIM2_IC | TIM2_PWM   
    GPIO_InitTypeDef GPIO_InitStructure;
#endif
    
#if TIM2_ITEN//中断 
	NVIC_InitTypeDef NVIC_InitStructure;
#endif
    
#if TIM2_PWM//PWM
	TIM_OCInitTypeDef TIM_OCInitStructure;
#endif
    
#if TIM2_IC//输入捕获
    TIM_ICInitTypeDef  TIM_ICInitStructure;
#endif
    
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE); //时钟TIM2使能
    //配置TIM2
    TIM_TimeBaseStructure.TIM_Period = TIM2Period;//自动重载计数值
	TIM_TimeBaseStructure.TIM_Prescaler = TIM2Prescaler;//分频系数 10kHz ---- 0.1ms 
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//时钟分频因子  不分频 系统时钟 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//计数方式 向上
	TIM_TimeBaseInit(TIM2,&TIM_TimeBaseStructure);//初始化

#if TIM2_IC//输入捕获  PA3
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO, ENABLE); //复用时钟使能
    GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_3;      //PA3 清除之前设置  
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;   //PA3 下拉输入 捕获高脉冲  
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	GPIO_SetBits(GPIOA,GPIO_Pin_3);
    //初始化TIM2输入捕获参数
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_4; //选择输入端
  	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;	//上升沿捕获
  	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI; //映射到TI1上
  	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;	 //对输入分频,不分频 
  	TIM_ICInitStructure.TIM_ICFilter = 0x03;    //IC4F=0011 配置输入滤波器 8个定时器时钟周期滤波
  	TIM_ICInit(TIM2, &TIM_ICInitStructure);
    TIM_ClearFlag(TIM2,TIM_FLAG_CC4);      	    //清除中断标志
    TIM_ITConfig(TIM2,TIM_IT_CC4,ENABLE); 	    //允许捕获中断 
#endif

#if TIM2_ITEN //中断
	TIM_ClearFlag(TIM2, TIM_FLAG_Update);      	//清除中断标志
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE); 	//允许更新中断 

	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn; //TIM3 中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02; //先占优先级 2 级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00; //从优先级 0 级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ 通道被使能
	NVIC_Init(&NVIC_InitStructure); //初始化 NVIC 寄存器
#endif
	TIM_Cmd(TIM2,ENABLE); //使能 TIMx 外设
}

//
void TIM3Init(uint16_t TIM3Period,uint16_t TIM3Prescaler)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    
#if TIM3_IC | TIM3_PWM   
    GPIO_InitTypeDef GPIO_InitStructure;
#endif
    
#if TIM3_ITEN//中断 
	NVIC_InitTypeDef NVIC_InitStructure;
#endif
    
#if TIM3_PWM//PWM
	TIM_OCInitTypeDef TIM_OCInitStructure;
#endif
    
#if TIM3_IC//输入捕获
    TIM_ICInitTypeDef  TIM_ICInitStructure;
#endif
    
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE); //时钟TIM3使能
    //配置TIM3    TIM_TimeBaseStructure.TIM_Period = TIM3Period;//自动重载计数值
	TIM_TimeBaseStructure.TIM_Prescaler = TIM3Prescaler;//分频系数 10kHz ---- 0.1ms 
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//时钟分频因子  不分频 系统时钟 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//计数方式 向上
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseStructure);//初始化

#if TIM3_PWM //PWM  PB5
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_AFIO, ENABLE); //复用时钟使能
	GPIO_PinRemapConfig(GPIO_PartialRemap_TIM3, ENABLE); //②部分重映射 TIM3_CH2->PB5
	//设置该引脚为复用输出功能,输出 TIM3 CH2 的 PWM 脉冲波形 GPIOB.5
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5; //TIM_CH2
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; //复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);//①初始化 GPIO
    //初始化 TIM3 Channel2 PWM 模式
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2; //选择 PWM模式2 
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low; //输出极性低
	TIM_OC2Init(TIM3,&TIM_OCInitStructure); //④初始化外设 TIM3 OC2
	TIM_OC2PreloadConfig(TIM3,TIM_OCPreload_Enable); //使能预装载寄存器
#endif

#if TIM3_IC//输入捕获  PB4
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_AFIO, ENABLE); //复用时钟使能    GPIO_PinRemapConfig(GPIO_PartialRemap_TIM3, ENABLE); //②部分重映射 TIM3_CH1->PB4
    GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_4;      //PB4 清除之前设置  
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;   //PB4 上拉输入 捕获低脉冲  
	GPIO_Init(GPIOB,&GPIO_InitStructure);
	GPIO_SetBits(GPIOB,GPIO_Pin_4);
    //初始化TIM3输入捕获参数
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_1; //CC1S=01 	选择输入端 IC1映射到TI1上
  	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Falling;	//下降沿捕获
  	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI; //映射到TI1上
  	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;	 //对输入分频,不分频 
  	TIM_ICInitStructure.TIM_ICFilter = 0x00;    //IC1F=0000 配置输入滤波器 不滤波
  	TIM_ICInit(TIM3, &TIM_ICInitStructure);    TIM_ClearFlag(TIM3,TIM_FLAG_CC1);      	    //清除中断标志
    TIM_ITConfig(TIM3,TIM_IT_CC1,ENABLE); 	    //允许捕获中断 
#endif

#if TIM3_ITEN //中断
	TIM_ClearFlag(TIM3, TIM_FLAG_Update);      	//清除中断标志
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE); 	//允许更新中断 

	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn; //TIM3 中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02; //先占优先级 2 级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03; //从优先级 3 级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ 通道被使能
	NVIC_Init(&NVIC_InitStructure); //初始化 NVIC 寄存器
#endif
	TIM_Cmd(TIM3,ENABLE); //使能 TIMx 外设
}


//
void TIM4Init(uint16_t TIM4Period,uint16_t TIM4Prescaler)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
#if TIM4_ITEN
	NVIC_InitTypeDef 		NVIC_InitStructure;
#endif

#if TIM4_CKMD    
    //PB6 -> FG 电机速度反馈 脉冲12/r
    GPIO_InitTypeDef GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO,ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 	  //上拉输入
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //50M时钟速度
	GPIO_Init(GPIOB,&GPIO_InitStructure);
#endif    

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE); //时钟使能
	TIM_TimeBaseStructure.TIM_Period = TIM4Period;//自动重载计数值
	TIM_TimeBaseStructure.TIM_Prescaler = TIM4Prescaler;//分频系数 10kHz ---- 0.1ms 
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//时钟分频因子  不分频 系统时钟 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//计数方式 向上
	TIM_TimeBaseInit(TIM4,&TIM_TimeBaseStructure);

#if TIM4_CKMD    
    TIM_TIxExternalClockConfig(TIM4,TIM_TIxExternalCLK1Source_TI1,TIM_ICPolarity_Rising,0);//CH1 外部计数模式 上升沿
    TIM_SetCounter(TIM4,0);//计数器清0 TIM_GetCounter(TIM4);
#endif

#if TIM4_ITEN
	TIM_ITConfig(TIM4,TIM_IT_Update,ENABLE ); 	//允许更新中断

	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn; //TIM4 中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01; //先占优先级 1 级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02; //从优先级 3 级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ 通道被使能
	NVIC_Init(&NVIC_InitStructure); //初始化 NVIC 寄存器
#endif
    TIM_ClearFlag(TIM4, TIM_FLAG_Update);      	//清除中断标志
	TIM_Cmd(TIM4, ENABLE); //使能 TIMx 外设
}

/*--------------------------TIM1溢出中断服务函数-------------------------------*/
#if TIM1_ITEN
void TIM1_UP_IRQHandler(void) //TIM1 中断
{
    static u8 i = 0;
    if (TIM_GetITStatus(TIM1,TIM_IT_Update) != RESET) //检查 TIM1 更新中断发生与否
	{
        TIM1ITCount1 ++;//电机测速
        TIM1ITCount2 ++;//按键扫描
        TIM1ITCount3 ++;//屏幕刷新
        TIM1ITCount4 ++;//净化闪烁
        TIM1ITCount5 ++;//倒计时
        TIM1ITCount6 ++;//滤网
        TIM1ITCount7 ++;//加湿
        if(TIM1ITCount1  > 100)//1S
        {    
            TIM1ITCount1 = 0;
            //计数 测电机速度
            airclearer.motospeed = (u16)((TIM_GetCounter(TIM4))/12.0);
            TIM_SetCounter(TIM4,0);
//            printf("%d r/min\n",airclearer.motospeed);
        }
        if(airclearer.keystatus)   { FMQOn(); i ++;}
        if(i > 8)   {FMQOff();i = 0;airclearer.keystatus = 0;}
        TIM_ClearITPendingBit(TIM1,TIM_IT_Update); //清除 TIM1 更新中断标志
	}
}
#endif

/*--------------------------TIM2中断服务函数-------------------------------*/
#if TIM2_ITEN
//遥控器接收状态
//[7]:收到了引导码标志
//[6]:得到了一个按键的所有信息
//[5]:保留	
//[4]:标记上升沿是否已经被捕获								   
//[3:0]:溢出计时器
u8 	RmtSta=0;	  	  
u16 Dval;		//下降沿时计数器的值
u32 RmtRec=0;	//红外接收到的数据	   		    
u8  RmtCnt=0;	//按键按下的次数
void TIM2_IRQHandler(void) //TIM2 中断
{	    				
    //10ms 溢出中断
    if(TIM_GetITStatus(TIM2,TIM_IT_Update) != RESET)
    {
        if(RmtSta & 0x80)//上次有数据被接收到了
        {	
            RmtSta &= 0XEF;						        //取消上升沿已经被捕获标记
            if((RmtSta&0X0F) == 0X00)                   //标记已经完成一次按键的键值信息采集   
            {
                RmtSta |= 0X40;
            }                
            if((RmtSta&0X0F) < 14)      RmtSta++;
            else
            {
                RmtSta &= 0X7F; //清空引导标识
                RmtSta &= 0XF0;	//清空计数器	
            }						 	   	
        }
        TIM_ClearFlag(TIM2,TIM_IT_Update);        
    }
    //捕获中断
 	if(TIM_GetITStatus(TIM2,TIM_IT_CC4) != RESET)
	{	  
		if(RDATA)//上升沿捕获
		{
			TIM_OC4PolarityConfig(TIM2,TIM_ICPolarity_Falling);		//CC1P=1 设置为下降沿捕获				
	    	TIM_SetCounter(TIM2,0);	   	    //清空定时器值
			RmtSta |= 0X10;					//标记上升沿已经被捕获
        }
        else //下降沿捕获
		{			
            Dval = TIM_GetCapture4(TIM2);//读取CCR1也可以清CC1IF标志位
            TIM_OC4PolarityConfig(TIM2,TIM_ICPolarity_Rising); //CC4P=0	设置为上升沿捕获
			if(RmtSta & 0X10)					//完成一次高电平捕获 
			{
 				if(RmtSta & 0X80)//接收到了引导码
				{
					if(Dval>300 && Dval<800)			//560为标准值,560us
					{
						RmtRec <<= 1;	//左移一位.
						RmtRec |= 0;	//接收到0	   
					}
                    else if(Dval>1400 && Dval<1800)	    //1680为标准值,1680us
					{
						RmtRec <<= 1;	//左移一位.
						RmtRec |= 1;	//接收到1
					}
                    else if(Dval>2200 && Dval<2600)	    //得到按键键值增加的信息 2500为标准值2.5ms
					{
						RmtCnt ++; 		//按键次数增加1次
						RmtSta &= 0XF0;	//清空计时器		
					}
 				}
                else if(Dval>4200 && Dval<4700)		    //4500为标准值4.5ms
				{
					RmtSta |= 0X80;	//标记成功接收到了引导码
					RmtCnt = 0;		//清除按键次数计数器
				}						 
			}
			RmtSta &= 0XEF;
		}
        TIM_ClearFlag(TIM2,TIM_IT_CC4);
	}
}

//处理红外键盘
//返回值:
//0,没有任何按键按下
//其他,按下的按键键值.
u16 RemoteScan(void)
{        
	u16 sta = 0;       
    u8 t1 = 0,t2 = 0;  
	if(RmtSta & 0x40)//得到一个按键的所有信息了
	{
        /*部分遥控器 前两个字节不是相反关系 地址码亦不确定 此处暂不作此判断*/        
//	    t1 = RmtRec>>24;			    //得到地址码
//	    t2 = (RmtRec>>16) & 0xff;	    //得到地址反码
// 	    if((t1 == (u8)~t2) && (t1 == REMOTE_ID))//检验遥控识别码(ID)及地址 
	    { 
	        t1 = RmtRec >> 8;
	        t2 = RmtRec; 	
	        if(t1 == (u8)~t2)   sta = t1;//键值正确
            
		}   
		if((sta == 0) || ((RmtSta&0X80) == 0))//按键数据错误/遥控已经没有按下了
		{
		 	RmtSta &= 0xBF; //清除接收到有效按键标识
			RmtCnt = 0;		//清除按键次数计数器
		}
	}
    airclearer.Remotekeyvaluenew = sta;
    switch(sta)
    {
        case 0x80: sta = SWITCHKEY;             break;
        case 0xea: sta = TIMERPLUSKEY;          break;
        case 0x50: sta = FOGAMOUNTKEY;          break;
        case 0x52: sta = WINDVELOCITYPLUSKEY;   break;
        case 0x08: sta = HUMIDIFICATIONKEY;     break;
        case 0x20: sta = CHILDLOCKKEY;          break;
        case 0xaa: sta = FILTER1KEY;            break;
        case 0x2a: sta = FILTER2KEY;            break;
        case 0x7a: sta = ANIONKEY;              break;
        case 0x30: sta = TIMERSUBKEY;           break;
        case 0x56: sta = MUTEKEY;               break;
        case 0x92: sta = WINDVELOCITYSUBKEY;    break;
        case 0x1a: sta = OPENLCDKEY;            break;
        case 0xfa: sta = CLOSELCDKEY;           break;
        
        default: sta = NOEFFECTKEY;             break;
    }
    return sta;
}
#endif

/*--------------------------TIM3中断服务函数-------------------------------*/
#if TIM3_ITEN

u16 TIM3CH1_CAPTURE_STA = 0;//输入捕获状态
u32	TIM3CH1_CAPTURE_VAL = 0;//TIM3_CH1输入捕获值

void TIM3_IRQHandler(void) //TIM3 中断
{	    				
    //定时10mS溢出中断 
    if(TIM_GetITStatus(TIM3,TIM_IT_Update) != RESET)//检查 TIM3 更新中断发生与否
    {	    
        if(TIM3CH1_CAPTURE_STA & 0X4000)//已经捕获到下降沿了
        {
            if((TIM3CH1_CAPTURE_STA & 0X0064) == 0X0064)//低电平太长了 100次1S
            {
                TIM3CH1_CAPTURE_STA |= 0X8000;//标记成功捕获了一次
                TIM3CH1_CAPTURE_VAL = 1000000;//100%
            }
            else TIM3CH1_CAPTURE_STA ++;
        }
        TIM_ClearITPendingBit(TIM3,TIM_IT_Update); //清除 TIM3 更新中断标志 
    }
    //输入捕获中断
    if(TIM_GetITStatus(TIM3,TIM_IT_CC1) != RESET) //捕获1发生捕获事件
	{
        if((TIM3CH1_CAPTURE_STA & 0X8000) == 0) //还未捕获成功	
        {	
            if(TIM3CH1_CAPTURE_STA & 0X4000)		    //已捕获到一个下降沿 		
            {	  			
                TIM3CH1_CAPTURE_STA &= 0XBFFF;        //清除下降沿标志
                TIM3CH1_CAPTURE_STA |= 0X8000;		//标记成功捕获到一次上升沿
                TIM3CH1_CAPTURE_VAL = TIM_GetCapture1(TIM3) + (TIM3CH1_CAPTURE_STA & 0X0064) * 10000;
                TIM_OC1PolarityConfig(TIM3,TIM_ICPolarity_Falling); //CC1P=0 设置为下降沿捕获
            }
            else  								//还未开始,第一次捕获下降沿
            {
                TIM3CH1_CAPTURE_STA = 0;		//清空
                TIM3CH1_CAPTURE_VAL = 0;
                TIM_SetCounter(TIM3,0);
                TIM3CH1_CAPTURE_STA |= 0X4000;		//标记捕获到了下降沿
                TIM_OC1PolarityConfig(TIM3,TIM_ICPolarity_Rising);//CC1P=1 设置为上升沿捕获
            }
        }
        TIM_ClearITPendingBit(TIM3,TIM_IT_CC1); //清除 TIM3 CH1输入捕获标志
    }
}

//返回PM2.5 低脉冲率 4舍5入
//若返回0xff 则未读出数据
void GetPM25Value(void)
{
    if(TIM3CH1_CAPTURE_STA & 0x8000)
    {
        airclearer.fairquality = TIM3CH1_CAPTURE_VAL * 0.0001 + 0.5;
        
        airclearer.airquality = (u8)airclearer.fairquality;
        if(airclearer.airquality > 14)  airclearer.airquality = 14;
        airclearer.airquality = 14 - airclearer.airquality;
        TIM3CH1_CAPTURE_STA &= 0x3fff;//欲读取PM2.5时必须清除标志位，否则读取不到更新的数据  
    }
}
#endif

/*--------------------------TIM4中断服务函数-------------------------------*/
#if TIM4_ITEN
void TIM4_IRQHandler(void) //TIM4 中断
{
	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET) //检查 TIM4 更新中断发生与否
	{
		TIM_ClearITPendingBit(TIM4, TIM_IT_Update ); //清除 TIM4 更新中断标志
	}
}
#endif



