#ifndef _ADC_H
#define _ADC_H

#include "stm32f10x.h"
#include "system.h"
#include "delay.h"
#include "usart.h"


void ADCInit(void);
void ADCGPIOInit(void);
void  Adc_Configuration(void);
u16 Get_Adc(u8 ch);
u16 Get_Adc_Average(u8 ch,u8 times);

#endif
