#ifndef _EXIT_H_
#define _EXIT_H_

#include "stm32f10x.h"
#include "system.h"
#include "delay.h"

#include "usart.h"
#include "main.h"

#define LSCL     PAout(15)
#define LSDO     PBin(3)
#define RSCL     PBout(10)
#define RSDO     PBin(11)

extern u8 touchkey;//��������ֵ

void EXITInit(void);


#endif
