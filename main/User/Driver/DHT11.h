#ifndef _DHT11_H
#define _DHT11_H

#include "stm32f10x.h"
#include "delay.h"
#include "system.h"

//IO方向设置  PB2
#define DHT11_IO_IN()  {GPIOB->CRL &= 0XFFFFF0FF;GPIOB->CRL |= 0X00000800;}//输入
#define DHT11_IO_OUT() {GPIOB->CRL &= 0XFFFFF0FF;GPIOB->CRL |= 0X00000300;}//输出
////IO操作函数											   
#define	DHT11_DQ_OUT PBout(2) //数据端口	PB2 
#define	DHT11_DQ_IN  PBin(2)  //数据端口	PB2 

u8 DHT11_Init(void);//初始化DHT11
u8 DHT11_Read_Data(u8 *temp,u8 *humi);//读取温湿度
u8 DHT11_Read_Byte(void);//读出一个字节
u8 DHT11_Read_Bit(void);//读出一个位
u8 DHT11_Check(void);//检测是否存在DHT11
void DHT11_Rst(void);//复位DHT11 

#endif
