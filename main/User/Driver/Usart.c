#include "usart.h"

u8 USART_RX_BUF[64];     //接收缓冲,最大64个字节.
u8 R_num=0;
u8 RevFinsh = 0;//接收是1否0完成 


void Usart1_Init(u32 bound)
{
	//GPIO端口设置
	GPIO_InitTypeDef 	GPIO_InitStructure;
	USART_InitTypeDef 	USART_InitStructure;
	NVIC_InitTypeDef 	NVIC_InitStructure;
	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1|RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO, ENABLE);
	 //USART1_TX   PA.9
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	//USART1_RX	  PA.10
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);  
	
	//USART 初始化设置   
	USART_InitStructure.USART_BaudRate = bound;//一般设置为9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);
#if RX_ITEN
	//Usart1 NVIC 配置
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器USART1
//	USART_ClearFlag(USART1,USART_IT_RXNE);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//开启接收中断
#endif   
	USART_Cmd(USART1, ENABLE);                    //使能串口 

}

#if RX_ITEN
void USART1_IRQHandler(void)                	//串口1中断服务程序
{
	u8 Res;
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)  //接收中断
	{		
		Res = USART_ReceiveData(USART1);//(USART1->DR);	//读取接收到的数据				
		USART_RX_BUF[R_num] = Res;
		R_num ++;	
		if((USART_RX_BUF[R_num - 1] == ']') || (R_num >= 64))	
		{
			RevFinsh = 1;
		} 
		if(USART_RX_BUF[0] != '[')	
		{
			R_num = 0;
			memset(USART_RX_BUF,'\0',64);
		}		 
     } 
}
#endif

void USART1_SendOneByte(u8 ch)
{
	USART1->DR = ch;
	while((USART1->SR&0X40)==0);//等待发送结束
	Delay1us(100);
}

//int fputc(int ch, FILE *f)
//{
//	USART1->DR=(u8)ch;
//	while((USART1->SR&0X40)==0);
//	return ch;
//}

unsigned short CRC16_CHECK(unsigned char *Buf, unsigned short CRC_CNT)//6*2=12
{
    unsigned short CRC_Temp;
    unsigned char i,j;
    CRC_Temp = 0xffff;
    for (i = 0;i < CRC_CNT; i ++)
    {      
        CRC_Temp ^= Buf[i];
        for (j = 0;j < 8;j ++) 
        {
            if (CRC_Temp & 0x01)
                CRC_Temp = (CRC_Temp >>1 ) ^ 0xa001;
            else
                CRC_Temp = CRC_Temp >> 1;
        }
    }
    return(CRC_Temp);
}

void Send_Data(u16 Data[6])
{
	int temp0[6] = {0};
	unsigned int temp1[6] = {0};
	unsigned char databuf[14] = {0};
	unsigned char i;
	unsigned short CRC16 = 0;
	for(i = 0;i < 6;i ++)
	{   
		temp0[i]  = (int)Data[i];
		temp1[i] = (unsigned int)temp0[i];   
	}  
	for(i = 0;i < 6;i ++) 
	{
		databuf[i*2]   = (unsigned char)(temp1[i]%256);
		databuf[i*2+1] = (unsigned char)(temp1[i]/256);
	} 
	CRC16 = CRC16_CHECK(databuf,12);
	databuf[12] = CRC16 % 256;
	databuf[13] = CRC16 / 256;
	USART_ClearFlag(USART1,USART_FLAG_TC);  
	for(i = 0;i < 14;i ++)
	{
		USART1_SendOneByte(databuf[i]);
	}
}

 


