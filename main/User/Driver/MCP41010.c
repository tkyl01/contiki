#include "mcp41010.h"

void Mcp41010Init(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
 	
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB,ENABLE);	 //使能PB端口时钟
	
 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;	 
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推完输出
 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(GPIOA,&GPIO_InitStructure);				     //初始化IO口

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;	            
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推完输出
 	GPIO_Init(GPIOB,&GPIO_InitStructure);				     //初始化IO口
}

void Write2Mcp41010Data(unsigned char cmd,unsigned char dat)     //调整数字电位器
{
    unsigned char i;
    MCP41010_CS = 1; 
    MCP41010_CLK = 1;
    MCP41010_SI = 0; 
	MCP41010_CS = 0;
	Delay1us(1200);
	MCP41010_CLK = 0;
	Delay1us(1200);  
	for(i = 0;i < 8;i ++)          //写命令
	{ 
		if(cmd & 0x80)  MCP41010_SI = 1; else MCP41010_SI = 0;
        cmd = cmd << 1;
		Delay1us(2);
		MCP41010_CLK = 1;
		Delay1us(10);
		MCP41010_CLK = 0;
		Delay1us(10); 
	}
	for(i = 0;i < 8;i ++)          //写数据
	{ 
		if(dat & 0x80)  MCP41010_SI = 1; else MCP41010_SI = 0;
        dat = dat << 1;
		Delay1us(2);
		MCP41010_CLK = 1;
		Delay1us(10);
		MCP41010_CLK = 0;
		Delay1us(10); 
	}
    MCP41010_CS = 1;
	Delay1us(600);
}
