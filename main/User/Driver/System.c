 /**
  ******************************************************************************
  * @file    system.c
  * @author  N-K-J
  * @version V3.5.0
  * @date    2012/12/11
  * @brief   system.c
  ******************************************************************************
 **/
#include "system.h"

/************************************
name：void JTAG_Set(u8 mode)
功能：配置JTAG模式
输入：JTAG_ALL_ENABLE    0X00
	  JTAG_SWD_ENABLE    0X01
	  JTAG_ALL_DISABLE   0X02
返回：NO 
************************************/		  
void JTAG_Set(u8 mode)
{
	u32 temp;
	temp = mode;
	temp <<= 25;
	RCC->APB2ENR |= 1<<0;     //开启辅助时钟	   
	AFIO->MAPR &= 0XF8FFFFFF; //清除MAPR的[26:24]
	AFIO->MAPR |= temp;       //设置jtag模式
} 

/************************************
name：void RCC_ConfigSys(void)
功能：配置系统时钟,使用内部RC或使用锁相环 8*9 = 72MHz
输入：无
返回：成功返回SUCCESS 失败返回ERROR :ErrorStatus 
************************************/
ErrorStatus RCC_ConfigSys(void)
{
#if  USEHSEI//内部时钟
	RCC_DeInit(); //将外设RCC寄存器重设为缺省值 
	RCC_HSICmd(ENABLE); 
	while(RCC_GetFlagStatus(RCC_FLAG_HSIRDY)== RESET);//等待HSI就绪 
	RCC_HCLKConfig(RCC_SYSCLK_Div1);   	//设置AHB时钟（HCLK） RCC_SYSCLK_Div1——AHB时钟 = 系统时  
	RCC_PCLK2Config(RCC_HCLK_Div1);   	// 设置高速AHB时钟（PCLK2)RCC_HCLK_Div1——APB2时钟 = HCLK     
	RCC_PCLK1Config(RCC_HCLK_Div2); 	//设置低速AHB时钟（PCLK1）RCC_HCLK_Div2——APB1时钟 = HCLK / 2     
//	FLASH_SetLatency(FLASH_Latency_2);  //设置FLASH存储器延时时钟周期数FLASH_Latency_2  2延时周期   
//	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);  	//选择FLASH预取指缓存的模,预取指缓存使能 
	RCC_PLLConfig(RCC_PLLSource_HSI_Div2, RCC_PLLMul_16);	//设置PLL时钟源及倍频系数，频率为8Mhz/2*16=64Mhz    
	RCC_PLLCmd(ENABLE);   //使能PLL  
	while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET) ; 	//检查指定的RCC标志位(PLL准备好标志)设置与否    
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);  			//设置系统时钟（SYSCLK）   
	while(RCC_GetSYSCLKSource() != 0x08);     				//0x08：PLL作为系统时钟

	SystemCoreClockUpdate();//更新SystemCoreClock 的值
	return SUCCESS;
	
#else	//外部时钟
	
	ErrorStatus HSEStartUpStatus;	
	RCC_DeInit();/* RCC system reset(for debug purpose) */		
	RCC_HSEConfig(RCC_HSE_ON);/* Enable HSE 外部震荡 8MHZ*/		
	HSEStartUpStatus = RCC_WaitForHSEStartUp();/* Wait till HSE is ready */	
	if(HSEStartUpStatus == SUCCESS)
	{					
    	RCC_HCLKConfig(RCC_SYSCLK_Div1);/* HCLK = SYSCLK  AHB时钟对系统时钟1分频 72M*/    
    	RCC_PCLK2Config(RCC_HCLK_Div1); /* PCLK2 = HCLK 高速时钟APB2对AHB时钟1分频72M 定时器1、8使用的时钟和其他*/   
    	RCC_PCLK1Config(RCC_HCLK_Div2);	/* PCLK1 = HCLK/2 低速时钟APB1对AHB时钟2分频 最高可达36M 定时器2-7使用的时钟和其他*/
		RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);/* PLLCLK = 8MHz * 9 = 72 MHz */		  
		RCC_PLLCmd(ENABLE);/* Enable PLL */			
		while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)/* Wait till PLL is ready */
		{
		}		
		RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);/* Select PLL as system clock source */		
		while(RCC_GetSYSCLKSource() != 0x08)/* Wait till PLL is used as system clock source */
		{
		}
		return SUCCESS;
	}
	return ERROR;
#endif
}

//系统时钟配置，设计1ms产生一次中断
void SysTick_Configuration(void)
{
 	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);//AHB时钟除以8  
#if  USEHSEI//内部时钟
	SysTick_Config(8000);//重新装载值 1ms中断
#else	
	SysTick_Config(9000);//重新装载值 1ms中断
#endif
}

//中断向量分组设置
void NVIC_Configuration(void)
{
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);	//设置NVIC中断分组2:2位抢占优先级，2位响应优先级
}


