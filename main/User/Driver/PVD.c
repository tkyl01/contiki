#include "pvd.h"


void PVD_Init(void)
{  
    NVIC_InitTypeDef NVIC_InitStruct;
    EXTI_InitTypeDef EXTI_InitStructure;

    RCC_APB1PeriphClockCmd( RCC_APB1Periph_PWR, ENABLE); //_PWR, ENABLE); // 这个是必需的
    PWR_PVDLevelConfig(PWR_PVDLevel_2V9);//2.9V中断

    EXTI_ClearITPendingBit(EXTI_Line16);
    EXTI_InitStructure.EXTI_Line = EXTI_Line16; 
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt; 
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling; 
    EXTI_InitStructure.EXTI_LineCmd = ENABLE; 
    EXTI_Init(&EXTI_InitStructure);

    NVIC_InitStruct.NVIC_IRQChannel = PVD_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x01;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;

    NVIC_Init(&NVIC_InitStruct);//允许中断
    
    PWR_PVDCmd(ENABLE);
}

//PVD中断函数如下
void PVD_IRQHandler(void)
{
    if(EXTI_GetITStatus(EXTI_Line16) != RESET)
    {
        if(PWR_GetFlagStatus(PWR_FLAG_PVDO) != RESET)
        {
            SaveConfigandother();//保存配置
            PWR_ClearFlag(PWR_FLAG_PVDO);
        }
        EXTI_ClearITPendingBit(EXTI_Line16);
    }	
}






