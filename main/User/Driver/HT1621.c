#include "ht1621.h"


#define uchar unsigned char
#define uint  unsigned short

const uchar digital8[16][2] = { {0x0f,0x05},{0x06,0x00},{0x0b,0x06},{0x0f,0x02},    //0-3
                                {0x06,0x03},{0x0d,0x03},{0x0d,0x07},{0x07,0x00},    //4-7
                                {0x0f,0x07},{0x0f,0x03},{0x07,0x07},{0x0c,0x07},    //8-b
                                {0x09,0x05},{0x0e,0x06},{0x09,0x07},{0x01,0x07}};   //c-f

uchar COM[32] = {0};
void Ht1621GPIOInit(void)
{
	GPIO_InitTypeDef	GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOA, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_Init(GPIOB,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
}

/******************************************************
写数据函数,cnt为传送数据位数,数据传送为低位在前
*******************************************************/
void Ht1621WrnBit(uchar Data,uchar cnt)
{
	uchar i;
	for(i = 0;i < cnt;i ++)
	{
		if(Data&0x80)	DATA_HT1621 = 1; else DATA_HT1621 = 0;
		WR_HT1621 = 0;
		somenop();
		WR_HT1621 = 1;
		somenop();
		Data <<= 1;
	}
}

/********************************************************
函数名称：void Ht1621WrCmd(uchar Cmd)
功能描述: HT1621命令写入函数
全局变量：无
参数说明：Cmd为写入命令数据
返回说明：无
说 明：写入命令标识位100
********************************************************/
void Ht1621WrCmd(uchar Cmd)
{
	CS_HT1621 = 0;
	somenop();
	Ht1621WrnBit(0x80,4); //写入命令标志100
	Ht1621WrnBit(Cmd,8); //写入命令数据
	CS_HT1621 = 1;
	somenop();
}

/********************************************************
函数名称：void Ht1621WrOneData(uchar Addr,uchar Data)
功能描述: HT1621在指定地址写入数据函数
全局变量：无
参数说明：Addr为写入初始地址，Data为写入数据
返回说明：无
说 明：因为HT1621的数据位4位，所以实际写入数据为参数的后4位
********************************************************/
void Ht1621WrOneData(uchar Addr,uchar Data)
{
	CS_HT1621 = 0;
	Ht1621WrnBit(0xa0,3); //写入数据标志101
	Ht1621WrnBit(Addr << 2,6); //写入地址数据
	Ht1621WrnBit(Data << 4,4); //写入数据
	CS_HT1621 = 1;
	somenop();
}

/********************************************************
函数名称：void Ht1621Clear(void)
功能描述: HT1621清屏
全局变量：无
参数说明：无
返回说明：无
版 本：1.0
说 明：清除屏幕
********************************************************/
void Ht1621Clear(void)
{
	uchar i= 0;
	for(i = 0;i < 32;i ++)
	{
		Ht1621WrOneData(i,0x00);
	}
}

/********************************************************
函数名称：void Ht1621_Init(void)
功能描述: HT1621初始化
全局变量：无
参数说明：无
返回说明：无
版 本：1.0
说 明：初始化后，液晶屏所有字段均显示
********************************************************/
void Ht1621Init(void)
{
	Ht1621GPIOInit();
//	LED_HT1621 = 1;
	CS_HT1621 = 1;
	RD_HT1621 = 1;
	WR_HT1621 = 1;
	DATA_HT1621 = 1;
	Delay1ms(20); //延时使LCD工作电压稳定
	Ht1621WrCmd(BIAS);
	Ht1621WrCmd(RC256); //使用内部振荡器
	Ht1621WrCmd(SYSDIS);//关振系统荡器和LCD偏压发生器
	Ht1621WrCmd(WDTDIS);//禁止看门狗
	Ht1621WrCmd(SYSEN);//打开系统振荡器
	Ht1621WrCmd(LCDON);//打开LCD偏压
}

/*************************************************应用*********************************************/
//HT1621 空气质量显示//val    1-14级,0-只显示字体，小于0 -1 关闭所有，>=14全显，叠加递增方式
void DispAirQuality(u8 val)
{
    COM[S6] &= 0x08;
	switch(val)
	{
		case 0xff: COM[S3] = 0x00; COM[S4] = 0x00; COM[S5] = 0x00; COM[S6] &= 0x08; break; //清除
        case 0:  COM[S3] = 0x00; COM[S4] = 0x00; COM[S5] = 0x00; COM[S6] |= 0x04; break; //X1 
		case 1:  COM[S3] = 0x00; COM[S4] = 0x00; COM[S5] = 0x00; COM[S6] |= 0x06; break; //X1 S14
		case 2:  COM[S3] = 0x00; COM[S4] = 0x00; COM[S5] = 0x00; COM[S6] |= 0x07; break; //X1 S14~S13
		case 3:  COM[S3] = 0x00; COM[S4] = 0x00; COM[S5] = 0x08; COM[S6] |= 0x07; break; //X1 S14~S12
		case 4:  COM[S3] = 0x00; COM[S4] = 0x00; COM[S5] = 0x0c; COM[S6] |= 0x07; break; //X1 S14~S11
		case 5:  COM[S3] = 0x00; COM[S4] = 0x00; COM[S5] = 0x0e; COM[S6] |= 0x07; break; //X1 S14~S10
		case 6:  COM[S3] = 0x00; COM[S4] = 0x00; COM[S5] = 0x0f; COM[S6] |= 0x07; break; //X1 S14~S9
		case 7:  COM[S3] = 0x00; COM[S4] = 0x08; COM[S5] = 0x0f; COM[S6] |= 0x07; break; //X1 S14~S8
		case 8:  COM[S3] = 0x00; COM[S4] = 0x0c; COM[S5] = 0x0f; COM[S6] |= 0x07; break; //X1 S14~S7
		case 9:  COM[S3] = 0x00; COM[S4] = 0x0e; COM[S5] = 0x0f; COM[S6] |= 0x07; break; //X1 S14~S6
		case 10: COM[S3] = 0x00; COM[S4] = 0x0f; COM[S5] = 0x0f; COM[S6] |= 0x07; break; //X1 S14~S5
		case 11: COM[S3] = 0x08; COM[S4] = 0x0f; COM[S5] = 0x0f; COM[S6] |= 0x07; break; //X1 S14~S4
		case 12: COM[S3] = 0x0c; COM[S4] = 0x0f; COM[S5] = 0x0f; COM[S6] |= 0x07; break; //X1 S14~S3
		case 13: COM[S3] = 0x0e; COM[S4] = 0x0f; COM[S5] = 0x0f; COM[S6] |= 0x07; break; //X1 S14~S2
		case 14: COM[S3] = 0x0f; COM[S4] = 0x0f; COM[S5] = 0x0f; COM[S6] |= 0x07; break; //X1 S14~S1
	}
	Ht1621WrOneData(S3,COM[S3]);
	Ht1621WrOneData(S4,COM[S4]);
	Ht1621WrOneData(S5,COM[S5]);
	Ht1621WrOneData(S6,COM[S6]);
}

//HT1621 甲醛含量显示
//val    1-14级,0-只显示字体，小于0 -1 关闭所有，>=14全显，叠加递增方式
void DispFormaldehydeContent(u8 val)
{
	COM[S10] &= 0x08;
	switch(val)
	{
		case 0xff: COM[S13] = 0x00; COM[S12] = 0x00; COM[S11] = 0x00; COM[S10] &= 0x08; break; //清除
        case 0:  COM[S13] = 0x00; COM[S12] = 0x00; COM[S11] = 0x00; COM[S10] |= 0x04; break; //X2 
		case 1:  COM[S13] = 0x00; COM[S12] = 0x00; COM[S11] = 0x00; COM[S10] |= 0x06; break; //X2 S28
		case 2:  COM[S13] = 0x00; COM[S12] = 0x00; COM[S11] = 0x00; COM[S10] |= 0x07; break; //X2 S28~S27
		case 3:  COM[S13] = 0x00; COM[S12] = 0x00; COM[S11] = 0x08; COM[S10] |= 0x07; break; //X2 S28~S26
		case 4:  COM[S13] = 0x00; COM[S12] = 0x00; COM[S11] = 0x0c; COM[S10] |= 0x07; break; //X2 S28~S25
		case 5:  COM[S13] = 0x00; COM[S12] = 0x00; COM[S11] = 0x0e; COM[S10] |= 0x07; break; //X2 S28~S24
		case 6:  COM[S13] = 0x00; COM[S12] = 0x00; COM[S11] = 0x0f; COM[S10] |= 0x07; break; //X2 S28~S23
		case 7:  COM[S13] = 0x00; COM[S12] = 0x08; COM[S11] = 0x0f; COM[S10] |= 0x07; break; //X2 S28~S22
		case 8:  COM[S13] = 0x00; COM[S12] = 0x0c; COM[S11] = 0x0f; COM[S10] |= 0x07; break; //X2 S28~S21
		case 9:  COM[S13] = 0x00; COM[S12] = 0x0e; COM[S11] = 0x0f; COM[S10] |= 0x07; break; //X2 S28~S20
		case 10: COM[S13] = 0x00; COM[S12] = 0x0f; COM[S11] = 0x0f; COM[S10] |= 0x07; break; //X2 S28~S19
		case 11: COM[S13] = 0x08; COM[S12] = 0x0f; COM[S11] = 0x0f; COM[S10] |= 0x07; break; //X2 S28~S18
		case 12: COM[S13] = 0x0c; COM[S12] = 0x0f; COM[S11] = 0x0f; COM[S10] |= 0x07; break; //X2 S28~S17
		case 13: COM[S13] = 0x0e; COM[S12] = 0x0f; COM[S11] = 0x0f; COM[S10] |= 0x07; break; //X2 S28~S16
		case 14: COM[S13] = 0x0f; COM[S12] = 0x0f; COM[S11] = 0x0f; COM[S10] |= 0x07; break; //X2 S28~S15
	}
	Ht1621WrOneData(S10,COM[S10]);
	Ht1621WrOneData(S11,COM[S11]);
	Ht1621WrOneData(S12,COM[S12]);
	Ht1621WrOneData(S13,COM[S13]);
}

//HT1621 风速显示
//val    1-5级,0-清除，>=5全显
void DispWindVelocity(uchar val)
{
	if(val > 5)		val = 5;
	switch(val)
	{
		case 0: COM[S7] = 0x00; COM[S6] &= 0x07; break;//清除
		case 1: COM[S7] = 0x00; COM[S6] |= 0x08; break;//T5
		case 2: COM[S7] = 0x08; COM[S6] &= 0x07; break;//T4
		case 3: COM[S7] = 0x04; COM[S6] &= 0x07; break;//T3
		case 4: COM[S7] = 0x02; COM[S6] &= 0x07; break;//T2
		case 5: COM[S7] = 0x01; COM[S6] &= 0x07; break;//T1
	}
	Ht1621WrOneData(S6,COM[S6]);
	Ht1621WrOneData(S7,COM[S7]);	
}

//HT1621 滤网显示
//val    低四位L1，高四位L2, >0显示，=0清除
void DispFilter(uchar val)
{
	if(val & 0x0f)  COM[S31] |= 0x01;    else COM[S31] &= 0x0e;//L1滤网2
    if(val & 0xf0)  COM[S9] |= 0x01;     else COM[S9] &= 0x0e; //L2滤网1    
	Ht1621WrOneData(S31,COM[S31]);
	Ht1621WrOneData(S9,COM[S9]);	
}

//HT1621 颜色条显示
//val    3级，按位显示
void DispColorBar(uchar val)
{
	COM[S8] &= 0x08;
    if((val & 0x07) == 0x00)  COM[S8] &= 0x08;//清除
    if((val & 0x01) == 0x01)  COM[S8] |= 0x01;//T6
    if((val & 0x02) == 0x02)  COM[S8] |= 0x02;//T7
    if((val & 0x04) == 0x04)  COM[S8] |= 0x04;//T8    
	Ht1621WrOneData(S8,COM[S8]);	
}

//HT1621 童锁显示
//val    1显示，0清除
void DispChildLock(uchar val)
{
    if(val)   COM[S14] |= 0x01;//X23 显示
    else      COM[S14] &= 0x0e;//X23 清除
	Ht1621WrOneData(S14,COM[S14]);	
} 

//HT1621 净化显示
//val    1-9级,0-只显示字体，小于0 -1 关闭所有，>=9全显，叠加递增方式
void DispPurify(u8 val)
{
	COM[S14] &= 0x09;

	switch(val)
	{
		case 0xff: COM[S16] = 0x00; COM[S15] = 0x00; COM[S14] &= 0x09; break; //清除
        case 0:  COM[S16] = 0x00; COM[S15] = 0x00; COM[S14] |= 0x02; break; //X22 
		case 1:  COM[S16] = 0x00; COM[S15] = 0x00; COM[S14] |= 0x06; break; //X22~X21
		case 2:  COM[S16] = 0x00; COM[S15] = 0x08; COM[S14] |= 0x06; break; //X22~X20
		case 3:  COM[S16] = 0x00; COM[S15] = 0x0c; COM[S14] |= 0x06; break; //X22~X19
		case 4:  COM[S16] = 0x00; COM[S15] = 0x0e; COM[S14] |= 0x06; break; //X22~X18
		case 5:  COM[S16] = 0x00; COM[S15] = 0x0f; COM[S14] |= 0x06; break; //X22~X17
		case 6:  COM[S16] = 0x08; COM[S15] = 0x0f; COM[S14] |= 0x06; break; //X22~X16
		case 7:  COM[S16] = 0x0c; COM[S15] = 0x0f; COM[S14] |= 0x06; break; //X22~X15
		case 8:  COM[S16] = 0x0e; COM[S15] = 0x0f; COM[S14] |= 0x06; break; //X22~X14
		case 9:  COM[S16] = 0x0f; COM[S15] = 0x0f; COM[S14] |= 0x06; break; //X22~X13		
	}
	Ht1621WrOneData(S14,COM[S14]);
	Ht1621WrOneData(S15,COM[S15]);
	Ht1621WrOneData(S16,COM[S16]);
}

//HT1621 加湿显示
//val    1-3级,0-只显示字体，小于0 -1 关闭所有，>=3全显，叠加递增方式
void DispHumidification(u8 val)
{
	COM[S29] &= 0x00;
	switch(val)
	{
		case 0xff: COM[S29] &= 0x00; break; //清除
        case 0:  COM[S29] |= 0x08; break; //X12
		case 1:  COM[S29] |= 0x0a; break; //X12,X10
		case 2:  COM[S29] |= 0x0e; break; //X12,X11,X10
		case 3:  COM[S29] |= 0x0f; break; //X12,X11,X10,X9		
	}
	Ht1621WrOneData(S29,COM[S29]);
}

//HT1621 负离子显示
//val    1-3级,0-只显示字体，小于0 -1 关闭所有，>=3全显，叠加递增方式
void DispAnion(u8 val)
{
	COM[S30] &= 0x00;
	switch(val)
	{
		case 0xff: COM[S30] &= 0x00; break; //清除
        case 0:  COM[S30] |= 0x08; break; //X8 
		case 1:  COM[S30] |= 0x0c; break; //X8,X7
		case 2:  COM[S30] |= 0x0e; break; //X8,X7,X6
		case 3:  COM[S30] |= 0x0f; break; //X8,X7,X6,X5		
	}
	Ht1621WrOneData(S30,COM[S30]);
} 

//HT1621    定时显示
//hour      小时
//minute    分钟
//mode      1 显示  0不显示
void DispTiming(uchar hour,uchar minute,uchar mode)
{
    COM[S28] &= 0x00;COM[S27] &= 0x00;COM[S26] &= 0x00;COM[S25] &= 0x00;
    COM[S24] &= 0x00;COM[S23] &= 0x00;COM[S22] &= 0x00;COM[S21] &= 0x00;
    if(mode)
    {
        if(minute > 59) {minute = 0;hour ++;}
        if(hour > 99)   hour = 0;
        COM[S28] |= 0x08;COM[S26] |= 0x08;COM[S24] |= 0x08;COM[S22] |= 0x08;
        
        COM[S28] |= digital8[hour / 10][1];
        COM[S27] |= digital8[hour / 10][0];
        COM[S26] |= digital8[hour % 10][1];
        COM[S25] |= digital8[hour % 10][0];
        
        COM[S24] |= digital8[minute / 10][1];
        COM[S23] |= digital8[minute / 10][0];
        COM[S22] |= digital8[minute % 10][1];
        COM[S21] |= digital8[minute % 10][0];
    }   
    Ht1621WrOneData(S28,COM[S28]);
    Ht1621WrOneData(S27,COM[S27]);
    Ht1621WrOneData(S26,COM[S26]);
    Ht1621WrOneData(S25,COM[S25]);
    Ht1621WrOneData(S24,COM[S24]);
    Ht1621WrOneData(S23,COM[S23]);
    Ht1621WrOneData(S22,COM[S22]);
    Ht1621WrOneData(S21,COM[S21]);
}

//HT1621    湿度显示
//val       0-100
//mode      1 显示  0不显示 
void DispHumidity(uchar val,uchar mode)
{  
    COM[S20] &= 0x00;COM[S19] &= 0x00;COM[S18] &= 0x00;COM[S17] &= 0x00; COM[S26] &= 0x07;
    if(mode)
    {
        if(val > 100) val = 100;
        
        COM[S20] |= 0x08;COM[S18] |= 0x08; COM[S26] |= 0x08;
        
        COM[S20] |= digital8[val / 10][1];
        COM[S19] |= digital8[val / 10][0];
        COM[S18] |= digital8[val % 10][1];
        COM[S17] |= digital8[val % 10][0];
    }   
    Ht1621WrOneData(S20,COM[S20]);
    Ht1621WrOneData(S19,COM[S19]);
    Ht1621WrOneData(S18,COM[S18]);
    Ht1621WrOneData(S17,COM[S17]);
    
    Ht1621WrOneData(S26,COM[S26]);
}





