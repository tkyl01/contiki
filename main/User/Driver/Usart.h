#ifndef _USART_H
#define _USART_H

#include "stm32f10x.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "Delay.h"

#define RX_ITEN  1 // 接收中断使能

extern u8 USART_RX_BUF[64];     //接收缓冲,最大63个字节.末字节为换行符 	
extern u8 RevFinsh;//接收是1否0完成
extern u8 R_num;

void Usart1_Init(u32 bound);
void USART1_SendOneByte(u8 ch);
unsigned short CRC16_CHECK(unsigned char *Buf, unsigned short CRC_CNT);
void Send_Data(u16 Data[6]);

#endif


