#ifndef _HT1621_H_
#define _HT1621_H_

#include "stm32f10x.h"
#include "system.h"
#include "delay.h"

#define   LED_HT1621	PBout(12)	//HT1621背光控制
#define   CS_HT1621		PBout(13)	//HT1621片选
#define   RD_HT1621		PBout(14)	//HT1621读选择端	
#define   WR_HT1621		PBout(15)	//HT1621写选择端	
#define   DATA_HT1621	PAout(8)	//HT1621数据端

#define somenop() 	Delay1us(10)

extern unsigned char COM[32];//HT1621寄存器暂存
#define C0	0x01
#define C1	0x02
#define C2	0x04
#define C3	0x08

#define S0	0
#define S1	1
#define S2	2
#define S3	3
#define S4	4
#define S5	5
#define S6	6
#define S7	7
#define S8	8
#define S9	9
#define S10	10
#define S11	11
#define S12	12
#define S13	13
#define S14	14
#define S15	15
#define S16	16
#define S17	17
#define S18	18
#define S19	19
#define S20	20
#define S21	21
#define S22	22
#define S23	23
#define S24	24
#define S25	25
#define S26	26
#define S27	27
#define S28	28
#define S29	29
#define S30	30
#define S31	31


#define BIAS 	0x52 		//0b1000 0101 0010 1/3duty 4com

#define SYSDIS 	0X00 		//0b1000 0000 0000 关振系统荡器和LCD偏压发生器
#define SYSEN 	0X02 		//0b1000 0000 0010 打开系统振荡器

#define LCDOFF 	0X04 		//0b1000 0000 0100 关LCD偏压
#define LCDON 	0X06 		//0b1000 0000 0110 打开LCD偏压
#define XTAL 	0x28 		//0b1000 0010 1000 外部接时钟
#define RC256 	0X30 		//0b1000 0011 0000 内部时钟
#define TONEON 	0X12 		//0b1000 0001 0010 打开声音输出
#define TONEOFF 0X10 		//0b1000 0001 0000 关闭声音输出
#define WDTDIS 	0X0A 		//0b1000 0000 1010 禁止看门狗
#define TNORMAL 0xE3


void Ht1621Init(void);											//1621初始化
void Ht1621Clear(void);											//1621清屏
void Ht1621WrnBit(unsigned char Data,unsigned char cnt);		//1621写入n个位
void Ht1621WrCmd(unsigned char Cmd);							//1621写入命令
void Ht1621WrOneData(unsigned char Addr,unsigned char Data);	//1621写入一个字节

void DispAirQuality(unsigned char val);							//显示空气质量
void DispFormaldehydeContent(unsigned char val);				//显示甲醛含量
void DispWindVelocity(unsigned char val);						//显示风速
void DispFilter(unsigned char val);                             //滤网
void DispColorBar(unsigned char val);                           //颜色条显示
void DispChildLock(unsigned char val);                          //童锁
void DispPurify(unsigned char val);                             //净化
void DispHumidification(unsigned char val);                     //加湿
void DispAnion(unsigned char val);                              //负离子
void DispTiming(unsigned char hour,unsigned char minute,unsigned char mode);       //定时void DispHumidity(unsigned char val,unsigned char mode);                           //湿度

#endif
