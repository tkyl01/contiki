#ifndef _TIM_H
#define _TIM_H

#include "stm32f10x.h"
#include "system.h"
#include "usart.h"
#include "main.h"

#define TIM1_ITEN   1    //使能TIM1中断

#define TIM2_ITEN   1    //使能TIM2中断
#define TIM2_PWM    0    //TIM2的PWM功能
#define TIM2_IC     1    //TIM2的输入捕获功能

#define TIM3_ITEN   1    //使能TIM3中断
#define TIM3_PWM    1    //TIM3的PWM功能
#define TIM3_IC     1    //TIM3的输入捕获功能

#define TIM4_ITEN   0    //使能TIM4中断
#define TIM4_CKMD   1    //使能TIM4外部计数模式


//红外遥控识别码(ID),每款遥控器的该值基本都不一样,但也有一样的.
//我们选用的遥控器识别码为0
#define REMOTE_ID 0 
#define RDATA PAin(3)	 //红外数据输入脚


extern u8 TIM1ITCount1;//定时器10ms中断计数1 侧电机转速使用
extern u8 TIM1ITCount2;//定时器10ms中断计数2 按键扫描使用
extern u8 TIM1ITCount3;//定时器10ms中断计数3 屏幕刷新使用extern u8 TIM1ITCount4;//定时器10ms中断计数4 
extern u16 TIM1ITCount5;//定时器10ms中断计数5
extern u8 TIM1ITCount6;//定时器10ms中断计数6
extern u8 TIM1ITCount7;//定时器10ms中断计数7

extern u16  TIM3CH1_CAPTURE_STA;//输入捕获状态
extern u32	TIM3CH1_CAPTURE_VAL;//TIM3_CH1输入捕获值

void TIM1Init(uint16_t TIM1Period,uint16_t TIM1Prescaler);
void TIM2Init(uint16_t TIM2Period,uint16_t TIM2Prescaler);
void TIM3Init(uint16_t TIM3Period,uint16_t TIM3Prescaler);
void TIM4Init(uint16_t TIM4Period,uint16_t TIM4Prescaler);

u16 RemoteScan(void);//得到红外键值
void GetPM25Value(void);//得到PM2.5值

#endif

