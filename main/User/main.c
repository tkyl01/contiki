/**
  ******************************************************************************
  * @file    main.c 
  * @author  NKJ@ZZ
  * @version V1.0.0
  * @date    20-10-2015
  * @brief   Main program body.
  ******************************************************************************
*/ 

#include "main.h"



int main(void)
{

    SystemInit();
    RCC_ConfigSys();  //时钟配置
    JTAG_Set(JTAG_SWD_ENABLE);//使能SWD 关闭 JTAG，解放 PA15，PB3，PB4
	NVIC_Configuration();//中断向量配置

    DelayInit(72);//延时72分频
	Usart1_Init(115200);printf(" Usart Init OK!\n");//串口配置

    GPIOInit();//控制 IO初始化

    printf("All OK!\n");
    Delay1ms(100);
	while(1)
	{
 
    }
}

void GPIOInit(void)
{
    GPIO_InitTypeDef	GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOA, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_4 | GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOA,&GPIO_InitStructure);


    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB,&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; //上拉输入
	GPIO_Init(GPIOB,&GPIO_InitStructure);
}

void SysTick_Handler(void)
{
//	current_clock++;  
//	if(etimer_pending()&& etimer_next_expiration_time()<= current_clock) 
//	{  
//		etimer_request_poll();  
//		// printf("%d,%d\n",clock_time(),etimer_next_expiration_time     ());  
//	}  
//	if (--second_countdown== 0) 
//	{  
//		current_seconds++;  
//		second_countdown = CLOCK_SECOND;  
//	} 
}

