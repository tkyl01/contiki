#include "stm32f10x.h"
#include <stdint.h>
#include <stdio.h>
#include <sys/process.h>
#include <sys/procinit.h>
#include <etimer.h>
#include <sys/autostart.h>
#include <clock.h>
#include "contiki.h"
#include "debug-uart.h"


unsigned int idle_count = 0;

//事件的声明 
static process_event_t event_data_ready;

//两个进程的声明
PROCESS(print_hello_process, "Hello");
PROCESS(print_world_process, "world");

//将两个进程加入AUTOSTART_PROCESSES
AUTOSTART_PROCESSES(&print_hello_process, &print_world_process);  


/*定义进程print_hello_process*/
PROCESS_THREAD(print_hello_process, ev, data)
{ 
	static struct etimer timer; 
	PROCESS_BEGIN();
	etimer_set(&timer, CLOCK_CONF_SECOND / 10); //#define CLOCK_CONF_SECOND 10将timer的interval设为1 
	printf("***print hello process start***\n");
	event_data_ready = process_alloc_event(); //return lastevent++; 新建一个事件，事实上是用一组unsigned char值来标识不同事件
	while(1)
	{
		PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER); //即etimer到期继续执行下面内容
		printf("Hello ");
		process_post(&print_world_process, event_data_ready, NULL); //传递异步事件给print_world_process，直到内核调度该进程才处理该事件。详情见4.3
		etimer_reset(&timer); //重置定时器
	}
	PROCESS_END();
}


/*定义进程print_world_process*/
PROCESS_THREAD(print_world_process, ev, data)
{
	PROCESS_BEGIN();
	printf("***print world process start***\n");
	while(1)
	{
		PROCESS_WAIT_EVENT_UNTIL(ev == event_data_ready);
		printf("world\n");
	}
	PROCESS_END();
}


int main(void)
{
	dbg_setup_uart(9600);
	printf("Initialising\n");
	clock_init();
	process_init();
	process_start(&etimer_process, NULL);
	autostart_start(autostart_processes);
	printf("Processes running\n");

	while(1) 
	{
		do 
		{
		} while(process_run() > 0);
		idle_count++;
		/* Idle! */
		/* Stop processor clock */
		/* asm("wfi"::); */ 
	}
	return 0;
}


